DROP TABLE IF EXISTS wine_inventory;

CREATE TABLE wine_inventory
        (
                inventory_id SERIAL NOT NULL
                , name varchar(25) NOT NULL
                , variety_id integer NOT NULL
                , vintage varchar(4) NOT NULL
                , producer varchar(25) NOT NULL
                , region varchar(25) NOT NULL
                , color_id integer NULL
                , acid_level long NULL
                , supplier_id VARCHAR(25) NULL
                , importer VARCHAR(25) NULL
                , country_id integer NULL
                , ava_id integer NULL
                , price money NULL
        );                 
                
                             