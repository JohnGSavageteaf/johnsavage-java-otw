package com.techelevator.controller;

import com.techelevator.model.CatCard;
import com.techelevator.model.CatCardDAO;
import com.techelevator.model.CatFact;
import com.techelevator.model.CatPic;
import com.techelevator.services.CatFactService;
import com.techelevator.services.CatPicService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/cards")
public class CatController {

    private CatCardDAO catCardDao;
    private CatFactService catFactService;
    private CatPicService catPicService;

                
    public CatController(CatCardDAO catCardDao, CatFactService catFactService, CatPicService catPicService) {
        this.catCardDao = catCardDao;
        this.catFactService = catFactService;
        this.catPicService = catPicService;
    }
    @RequestMapping(path = "/random", method = RequestMethod.GET)
    public CatCard randomCard()
    {
    	CatCard catCard = new CatCard();
    	CatFact catFact = catFactService.getFact();
    	CatPic catPic = catPicService.getPic();
    	catCard.setCatFact(catFact.getText());
    	catCard.setImgUrl(catPic.getFile());
    	return catCard;
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public List<CatCard> getAllCards() 
    {
        return catCardDao.list();
    } 
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public CatCard getIndividualCard(@PathVariable long id) 
    {
        return catCardDao.get(id);
    }
    
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public void saveNewCard(@Valid @RequestBody CatCard incomingCard) {
        catCardDao.save(incomingCard);
    }
    

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void updateExistingCard(@Valid @RequestBody CatCard changedCard, @PathVariable long id) 
    {
       catCardDao.update(id, changedCard);
    }
    
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id})", method = RequestMethod.DELETE)
    public void deleteCard(@PathVariable long id)
    {
    	if(catCardDao.get(id) != null)
    	{
    		catCardDao.delete(id);
    	}
    }
}
