package com.techelevator.views;

import java.util.List;

import com.techelevator.models.Product;

public class UserOutput
{
	public static void displayWelcome()
	{
		System.out.println("Welcome to The Vendor");
	}
	
	
			public static void displayProducts(List<Product> products)
			
			{
				System.out.println();
				System.out.println("----------------Products-------------");
				System.out.println();
				
				for (Product product : products)
				{
					System.out.println(product.getSlot() + " $" + product.getPrice());
					
				}
				System.out.println();
			}
			

}
