-- DDL - Data Definition Language
-- CREATE database, table
-- DROP database, table
-- ALTER table

-- connected to the POSTGRES database when you are creating another db

-- kill and db locks

--CREATE DATABASE project_manager;

CREATE TABLE employee
(
        -- column_name datatype NULL/NOTNULLconstraints
        employee_id SERIAL NOT NULL PRIMARY KEY
        , job_title VARCHAR(50) NOT NULL
        , first_name VARCHAR(50) NOT NULL
        , last_name VARCHAR(50) NOT NULL
        , gender VARCHAR (1) NOT NULL
        , date_of_birth DATE NOT NULL
        , date_of_hire DATE NOT NULL
        , department_id INTEGER  NOT NULL        
);

CREATE TABLE department
(
        department_id SERIAL NOT NULL PRIMARY KEY
        , name VARCHAR(50) NOT NULL 
);
CREATE TABLE project
(
        project_id SERIAL NOT NULL PRIMARY KEY
        , project_name VARCHAR(50) NOT NULL
        , name VARCHAR(50) NOT NULL
        , start_date DATE NOT NULL
);

-- START POPULATE DATA HERE !-------------------------------------------------------------------------------------------------
SELECT *
FROM employee;


INSERT INTO employee (job_title, first_name, last_name, gender, date_of_birth, date_of_hire, department_id)
        VALUES ('supervisor', 'AARON', 'ADAMS', 'M', '1965-03-08', '2000-06-01', '1');

INSERT INTO employee (job_title, first_name, last_name, gender, date_of_birth, date_of_hire, department_id)
        VALUES ('front_end', 'BETTY', 'BRITE', 'F', '1967-08-03', '2000-06-01', '1');
        
INSERT INTO employee (job_title, first_name, last_name, gender, date_of_birth, date_of_hire, department_id)
        VALUES ('floor_staff', 'CLIFF', 'CLAVEN', 'M', '1969-06-06', '2000-06-01', '2');        

INSERT INTO employee (job_title, first_name, last_name, gender, date_of_birth, date_of_hire, department_id)
        VALUES ('front_end', 'DANNY', 'DONUT', 'M', '1972-04-14', '2000-06-01', '3');
        
INSERT INTO employee (job_title, first_name, last_name, gender, date_of_birth, date_of_hire, department_id)
        VALUES ('supervisor', 'EVELYN', 'ELF', 'F', '1986-02-26', '2000-06-01', '3');
        
INSERT INTO employee (job_title, first_name, last_name, gender, date_of_birth, date_of_hire, department_id)
        VALUES ('front_end', 'FREDDY', 'FREELOADER', 'M', '19688-08-23', '2000-06-01', '2');
        
INSERT INTO employee (job_title, first_name, last_name, gender, date_of_birth, date_of_hire, department_id)
        VALUES ('floor_staff', 'GARY', 'GRUMP', 'M', '1992-09-18', '2000-06-01', '3');    

INSERT INTO employee (job_title, first_name, last_name, gender, date_of_birth, date_of_hire, department_id)
        VALUES ('front_end', 'HELEN', 'HAYES', 'F', '2000-11-11', '2010-06-01', '4');   

INSERT INTO employee (job_title, first_name, last_name, gender, date_of_birth, date_of_hire, department_id)
        VALUES ('floor_staff', 'IRENE', 'IVY', 'F', '2000-07-05', '2011-06-01', '2');          

INSERT INTO employee (job_title, first_name, last_name, gender, date_of_birth, date_of_hire, department_id)
        VALUES ('front_end', 'JAMES', 'JONES', 'M', '1902-09-09', '2015-06-01', '3'); 
        

        
-- INSERT DEPARTMENT---------------------------------------------------------

SELECT *                        
FROM department;

INSERT INTO department (department_id, name)
        VALUES ('1', 'front_desk');

INSERT INTO department (department_id, name)
        VALUES ('2', 'inventory');

INSERT INTO department (department_id, name)
        VALUES ('3', 'restock');                     

INSERT INTO department (department_id, name)
        VALUES ('4', 'floor_clean-up');        
        
--INSERT PROJECTS HERE------------------------------------------------------------

SELECT *
FROM project;

INSERT INTO project (project_name, name, start_date)
        VALUES ('floor_clean_up', 'cliff_claven', '2021-01-20');

INSERT INTO project (project_name, name, start_date)
        VALUES ('front_desk', 'gary_grump', '2021-01-20');
        
INSERT INTO project (project_name, name, start_date)
        VALUES ('inventory', 'helen_hayes', '2021-01-20');       
        
INSERT INTO project (project_name, name, start_date)
        VALUES ('restock', 'evelyn_elf', '2021-01-20');

INSERT INTO project (project_name, name, start_date)
        VALUES ('front_desk', 'irene_ivy', '2021-01-21');
        
INSERT INTO project (project_name, name, start_date)
        VALUES ('floor_clean_up', 'james_jones', '2021-01-21');

INSERT INTO project (project_name, name, start_date)
        VALUES ('restock', 'evelyn_elf', '2021-01-21');                                               