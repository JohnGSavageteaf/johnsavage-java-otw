
-- Write queries to return the following:
-- The following changes are applied to the "dvdstore" database.**

-- 1. Add actors, Hampton Avenue, and Lisa Byway to the actor table.
SELECT *
FROM actor;

INSERT INTO actor (first_name, last_name)
        VALUES ('HAMPTON', 'AVENUE');

INSERT INTO actor (first_name, last_name)
        VALUES ('LISA', 'BYWAY');
        
-- 2. Add "Euclidean PI", "The epic story of Euclid as a pizza delivery boy in
-- ancient Greece", to the film table. The movie was released in 2008 in English.
-- Since its an epic, the run length is 3hrs and 18mins. There are no special
-- features, the film speaks for itself, and doesn't need any gimmicks.
SELECT *
FROM film;

INSERT INTO film (title, description, release_year, language_id, length) 
VALUES ('EUCLIDEAN PI', 'A The Epic Story of Euclid as a Pizza Delivery Boy in Ancient Greece', '2008', '1', '138');

BEGIN TRANSACTION;

UPDATE film SET length = '198'
WHERE film_id = '1001';



-- 3. Hampton Avenue plays Euclid, while Lisa Byway plays his slightly
-- overprotective mother, in the film, "Euclidean PI". Add them to the film.
SELECT film_id
        , title
FROM film
WHERE film_id = 1001;

INSERT INTO film_actor (film_id, actor_id)
        VALUES ('201', '202');

-- 4. Add Mathmagical to the category table.
SELECT *
FROM category;

INSERT INTO category (name)
        VALUES ('Mathmagical');
-- 5. Assign the Mathmagical category to the following films, "Euclidean PI",
-- "EGG IGBY", "KARATE MOON", "RANDOM GO", and "YOUNG LANGUAGE"
SELECT *
FROM film_category
WHERE film_id = '274';

UPDATE film_category SET category_id = '17'
WHERE film_id = '494';

UPDATE film_category SET category_id = '17'
WHERE film_id = '714';

UPDATE film_category SET category_id = '17'
WHERE film_id = '996';

UPDATE film_category SET category_id = '17'
WHERE film_id = '1001';
-- 6. Mathmagical films always have a "G" rating, adjust all Mathmagical films
-- accordingly.
-- (5 rows affected)
SELECT title
        , rating
FROM film
WHERE film_id = '1001';

UPDATE film SET rating = 'G'
WHERE film_id = 274 OR film_id = 494 OR film_id = 714 OR film_id = 996 OR film_id = 1001;

-- 7. Add a copy of "Euclidean PI" to all the stores.
SELECT *
FROM inventory;

INSERT INTO inventory (film_id, store_id)
        VALUES ('1001', '1');

INSERT INTO inventory (film_id, store_id)
        VALUES ('1001', '2');
                
-- 8. The Feds have stepped in and have impounded all copies of the pirated film,
-- "Euclidean PI". The film has been seized from all stores, and needs to be
-- deleted from the film table. Delete "Euclidean PI" from the film table.
-- (Did it succeed? Why?)
BEGIN TRANSACTION;

        DELETE FROM film
        WHERE title = 'EUCLIDEAN PI';
        
        DELETE FROM inventory
        WHERE film_id = '1001';
        
        SELECT * 
        FROM film;
        
ROLLBACK TRANSACTION;
-- <YOUR ANSWER HERE>
-- NO [Code: 0, SQL State: 23503]  ERROR: update or delete on table "film" violates foreign key constraint "inventory_film_id_fkey" on table "inventory"
--  Detail: Key (film_id)=(1001) is still referenced from table "inventory".
  
-- 9. Delete Mathmagical from the category table.
-- (Did it succeed? Why?)
BEGIN TRANSACTION;

        DELETE FROM category
        WHERE name = 'Mathmagical';
        
        
ROLLBACK TRANSACTION;
-- <YOUR ANSWER HERE>
--NO AGAIN - [Code: 0, SQL State: 23503]  ERROR: update or delete on table "category" violates foreign key constraint "film_category_category_id_fkey" on table "film_category"
--Detail: Key (category_id)=(17) is still referenced from table "film_category".
  
-- 10. Delete all links to Mathmagical in the film_category tale.
-- (Did it succeed? Why?)
BEGIN TRANSACTION;

        DELETE FROM film_category
        WHERE category_id = '1001';
        
        
ROLLBACK TRANSACTION;
-- <YOUR ANSWER HERE>
--YES
 
-- 11. Retry deleting Mathmagical from the category table, followed by retrying
-- to delete "Euclidean PI".
-- (Did either deletes succeed? Why?)
-- <YOUR ANSWER HERE>
BEGIN TRANSACTION;

        DELETE FROM category
        WHERE name = 'Mathmagical';
        
        
ROLLBACK TRANSACTION;
BEGIN TRANSACTION;

        DELETE FROM film
        WHERE title = 'EUCLIDEAN PI';
        
ROLLBACK TRANSACTION;


-- <YOUR ANSWER HERE>
--NO [Code: 0, SQL State: 23503]  ERROR: update or delete on table "category" violates foreign key constraint "film_category_category_id_fkey" on table "film_category"
--Detail: Key (category_id)=(17) is still referenced from table "film_category".
-- NO [Code: 0, SQL State: 23503]  ERROR: update or delete on table "film" violates foreign key constraint "inventory_film_id_fkey" on table "inventory"
--Detail: Key (film_id)=(1001) is still referenced from table "inventory".  
  
  
-- 12. Check database metadata to determine all constraints of the film id, and
-- describe any remaining adjustments needed before the film "Euclidean PI" can
-- be removed from the film table.

--Will need to remove film_id constraints from film_actor, and inventory befor removing from the film table.  
--We have already removed it from the film_category table in exercise # 10.

