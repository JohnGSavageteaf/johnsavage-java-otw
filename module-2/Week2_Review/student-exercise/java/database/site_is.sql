INSERT INTO reservation (
        site_id
        , name
        , from_date
        , to_date
        , create_date
        )                   
        VALUES (?, ?, ?, ?, ?);       
        
SELECT reservation_id
FROM reservation
WHERE site_id = ?
        AND name = ?
        AND from_date = ?
        AND to_date = ?;