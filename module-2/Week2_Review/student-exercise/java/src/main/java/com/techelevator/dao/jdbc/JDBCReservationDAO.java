package com.techelevator.dao.jdbc;

import com.techelevator.dao.ReservationDAO;
import com.techelevator.model.Reservation;
import com.techelevator.model.Site;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class JDBCReservationDAO implements ReservationDAO {

    private JdbcTemplate jdbcTemplate;

    public JDBCReservationDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public int createReservation(int siteId, String name, LocalDate fromDate, LocalDate toDate) {
        
    	 String createNewReservation = "INSERT INTO reservation (\r\n " 
    	    		+ "        site_id\r\n "  
    	    		+ "        , name\r\n "  
    	    		+ "        , from_date\r\n "  
    	    		+ "        , to_date\r\n "  
    	    		+ "        , create_date\r\n "  
    	    		+ "        )                   \r\n "  
    	    		+ "        VALUES (?, ?, ?, ?, ?);";
    	    
    	 jdbcTemplate.update(createNewReservation, siteId, name, fromDate, toDate, LocalDateTime.now() );
    	
    	 //String p_diddy = "SELECT reservation_id\r\n "  
    	 //			+ " FROM reservation\r\n "  
    	 //			+ " WHERE site_id = ?\r\n "  
    	 //			+ "        AND name = ?\r\n "  
    	 //			+ "        AND from_date = ?\r\n "  
    	 //			+ "        AND to_date = ?;";
    	 
    	 
    	 //SqlRowSet jayz = jdbcTemplate.queryForRowSet(p_diddy, siteId, name, fromDate, toDate);
    	 
    	int reservation_id = 1;
    	 
    	 return reservation_id;
        
    }
    	
    @Override
    public List<Reservation> getAllReservationsNextThirtyDays(int parkId)
    {
    List<Reservation>reservations = new ArrayList<Reservation>();
    
    	String nextThirtyDays = "SELECT reservation_id \r\n "  
    			+"        , r.site_id\r\n "  
    			+"        , r.name\r\n "  
    			+"        , r.from_date\r\n "  
    			+"        , r.to_date\r\n "  
    			+"        ,r.create_date\r\n "  
    			+"FROM reservation AS r\r\n "  
    			+"INNER JOIN site AS s\r\n "  
    			+"        ON r.site_id = s.site_id\r\n "  
    			+"INNER JOIN campground AS c\r\n "  
    			+"        ON s.campground_id = c.campground_id        \r\n "  
    			+"WHERE from_date > current_date\r\n "  
    			+"        AND from_date <= current_date + 30\r\n "  
    			+"        AND park_id = ?;";
    
    	
    		SqlRowSet rows = jdbcTemplate.queryForRowSet(nextThirtyDays, parkId);
    	
    		while (rows.next())
    		{
    			Reservation reservation = mapRowToReservation(rows);
    		
    			reservations.add(reservation);
    		}
    	
    return reservations;
    }
     
    		
    		
    private Reservation mapRowToReservation(SqlRowSet results) {
        Reservation r = new Reservation();
        r.setReservationId(results.getInt("reservation_id"));
        r.setSiteId(results.getInt("site_id"));
        r.setName(results.getString("name"));
        r.setFromDate(results.getDate("from_date").toLocalDate());
        r.setToDate(results.getDate("to_date").toLocalDate());
        r.setCreateDate(results.getDate("create_date").toLocalDate());
        return r;
    }


}
