package com.techelevator.dao.jdbc;

import com.techelevator.dao.SiteDAO;
import com.techelevator.model.Site;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JDBCSiteDAO implements SiteDAO {

    private JdbcTemplate jdbcTemplate;

    public JDBCSiteDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Site> getSitesThatAllowRVs(int parkId) 
    {
    	String query = "SELECT site_id\r\n " 
    			+"        , s.campground_id\r\n "  
    			+"        , site_number\r\n "  
    			+"        , max_occupancy\r\n "  
    			+"        , accessible\r\n "  
    			+"        , max_rv_length\r\n " 
    			+"        , utilities\r\n " 
    			+"FROM site AS s\r\n " 
    			+"INNER JOIN campground AS c\r\n " 
    			+"ON s.campground_id = c.campground_id\r\n "
    			+"WHERE max_rv_length > 0\r\n "  
    			+"        AND park_id = ?;";
    	
    	SqlRowSet rows = jdbcTemplate.queryForRowSet(query, parkId);
    	
    	List<Site> sites = new ArrayList<Site>();
    	
    	while (rows.next())
    	{
    		Site site = mapRowToSite(rows);
    		
    		sites.add(site);
    		
    	}

    	return sites;
    }

    @Override
    public List<Site> getCurrentlyAvailableSites() 
    {
    
    	String query = "SELECT s.site_id\r\n" + 
    			"        , campground_id\r\n" + 
    			"        , site_number\r\n" + 
    			"        , max_occupancy\r\n" + 
    			"        , accessible\r\n" + 
    			"        , max_rv_length\r\n" + 
    			"        , utilities\r\n" + 
    			"FROM site AS s\r\n" + 
    			"left JOIN reservation AS r\r\n" + 
    			"        ON s.site_id = r.site_id\r\n" + 
    			"WHERE reservation_id IS NULL\r\n" + 
    			"        AND campground_id = ?\r\n" + 
    			"ORDER BY s.site_id\r\n" + 
    			"        , campground_id;";
    	
	SqlRowSet rows = jdbcTemplate.queryForRowSet(query);
    	
    	List<Site> sites = new ArrayList<Site>();
    	
    	while (rows.next())
    	{
    		Site site = mapRowToSite(rows);
    		
    		sites.add(site);
    		
    	}
    	
    	return sites;
    }
    
    private Site mapRowToSite(SqlRowSet results) {
        Site site = new Site();
        site.setSiteId(results.getInt("site_id"));
        site.setCampgroundId(results.getInt("campground_id"));
        site.setSiteNumber(results.getInt("site_number"));
        site.setMaxOccupancy(results.getInt("max_occupancy"));
        site.setAccessible(results.getBoolean("accessible"));
        site.setMaxRvLength(results.getInt("max_rv_length"));
        site.setUtilities(results.getBoolean("utilities"));
        return site;
    }
}
