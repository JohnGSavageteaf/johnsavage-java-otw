-- The following queries utilize the "dvdstore" database.

-- 1. All of the films that Nick Stallone has appeared in
-- (30 rows)
 
SELECT *
FROM film_actor
INNER JOIN film
        ON film_actor.film_id = film.film_id
WHERE actor_id = '44';

--SELECT f.title
--FROM actor AS a
--INNER JOIN film_actor AS fa
--        ON a.actor_id = fa.actor_id
--INNER JOIN film AS f
--        ON fa.film_id = f.film_id
--WHERE first_name = 'NICK' AND last_name = 'STALLONE';        



-- 2. All of the films that Rita Reynolds has appeared in
-- (20 rows)
SELECT *
FROM film_actor
INNER JOIN film
        ON film_actor.film_id = film.film_id
WHERE actor_id = '135';

-- 3. All of the films that Judy Dean or River Dean have appeared in
-- (46 rows)
SELECT *
FROM film_actor
INNER JOIN film
        ON film_actor.film_id = film.film_id
WHERE actor_id = '35' OR actor_id = '143';

-- 4. All of the the ‘Documentary’ films
-- (68 rows)
SELECT *
FROM film_category
INNER JOIN film
        ON film_category.film_id = film.film_id
WHERE category_id = '6';

-- 5. All of the ‘Comedy’ films
-- (58 rows)
SELECT *
FROM film_category
INNER JOIN film
        ON film_category.film_id = film.film_id
WHERE category_id = '5';

-- 6. All of the ‘Children’ films that are rated ‘G’
-- (10 rows)
SELECT *
FROM film_category
INNER JOIN film
        ON film_category.film_id = film.film_id
WHERE category_id = '3' AND film.rating = 'G';

-- 7. All of the ‘Family’ films that are rated ‘G’ and are less than 2 hours in length
-- (3 rows)
SELECT *
FROM film_category
INNER JOIN film
        ON film_category.film_id = film.film_id
WHERE category_id = '8' AND film.rating = 'G' AND film.length < 120;

-- 8. All of the films featuring actor Matthew Leigh that are rated ‘G’
-- (9 rows)
SELECT *
FROM film_actor
INNER JOIN film
        ON film_actor.film_id = film.film_id
WHERE actor_id = '103' AND film.rating = 'G';

-- 9. All of the ‘Sci-Fi’ films released in 2006
-- (61 rows)
SELECT *
FROM film_category
INNER JOIN film
        ON film_category.film_id = film.film_id
WHERE category_id = '14' AND film.release_year = '2006';

-- 10. All of the ‘Action’ films starring Nick Stallone
-- (2 rows)
SELECT *
FROM film
INNER JOIN film_actor
        ON film_actor.film_id = film.film_id
        JOIN film_category 
        ON film_category.film_id = film.film_id   
WHERE actor_id = '44' AND category_id = '1';

-- 11. The address of all stores, including street address, city, district, and country
-- (2 rows) 

SELECT address AS street_address
        ,district
        , city.city AS city
        , store.store_id
        , country.country        
FROM address
INNER JOIN city
        ON city.city_id = address.city_id
INNER JOIN country
        ON country.country_id = city.country_id
INNER JOIN store
        ON address.address_id = store.address_id;
      

-- 12. A list of all stores by ID, the store’s street address, and the name of the store’s manager
-- (2 rows)
SELECT store.store_id
        , address.address
        , store.manager_staff_id
        , staff.first_name
        , staff.last_name
FROM store
INNER JOIN address
        ON address.address_id = store.address_id
INNER JOIN city
        ON city.city_id = address.city_id
INNER JOIN staff
        ON store.store_id = staff.store_id;


-- 13. The first and last name of the top ten customers ranked by number of rentals 
-- (#1 should be “ELEANOR HUNT�? with 46 rentals, #10 should have 39 rentals)
SELECT COUNT (rental.inventory_id)
        , rental.customer_id
        , customer.first_name
        , customer.last_name
FROM rental
INNER JOIN customer
        ON customer.customer_id = rental.customer_id
GROUP BY rental.customer_id
        , customer.first_name
        , customer.last_name
ORDER BY count DESC LIMIT 10;
 

-- 14. The first and last name of the top ten customers ranked by dollars spent 
-- (#1 should be “KARL SEAL�? with 221.55 spent, #10 should be “ANA BRADLEY�? with 174.66 spent)
SELECT SUM(payment.amount)
        , customer.first_name
        , customer.last_name 
FROM payment
INNER JOIN customer
        ON customer.customer_id = payment.customer_id
GROUP BY customer.first_name
        , customer.last_name
        , payment.customer_id
ORDER BY SUM DESC LIMIT 10;

-- 15. The store ID, street address, total number of rentals, total amount of sales (i.e. payments), and average sale of each store.
-- (NOTE: Keep in mind that an employee may work at multiple stores.)
-- (Store 1 has 7928 total rentals and Store 2 has 8121 total rentals)

SELECT store.store_id
        , address.address
        , COUNT (inventory.inventory_id)
        , SUM(amount) AS total_sales
        , AVG(amount) AS avarage_sales
        , COUNT(rental.rental_id)
FROM store
INNER JOIN address
        ON store.address_id = address.address_id
INNER JOIN inventory
        ON store.store_id = inventory.store_id
INNER JOIN rental
        ON inventory.inventory_id = rental.inventory_id
INNER JOIN payment
        ON rental.rental_id = payment.rental_id
GROUP BY store.store_id
        , address.address;



-- 16. The top ten film titles by number of rentals
-- (#1 should be “BUCKET BROTHERHOOD�? with 34 rentals and #10 should have 31 rentals)
SELECT COUNT (title)
        , inventory.film_id
        , film.title
FROM inventory
INNER JOIN rental
        ON rental.inventory_id = inventory.inventory_id
INNER JOIN film
        ON film.film_id = inventory.film_id
GROUP BY inventory.film_id
        , film.title
ORDER BY count DESC LIMIT 10;

-- 17. The top five film categories by number of rentals 
-- (#1 should be “Sports�? with 1179 rentals and #5 should be “Family�? with 1096 rentals)

SELECT COUNT (title)
        , category.name       
FROM inventory
INNER JOIN rental
        ON rental.inventory_id = inventory.inventory_id
INNER JOIN film
        ON film.film_id = inventory.film_id
INNER JOIN film_category
        ON film.film_id = film_category.film_id
INNER JOIN category
        ON film_category.category_id = category.category_id
GROUP BY category.name
ORDER BY count DESC LIMIT 5;

-- 18. The top five Action film titles by number of rentals 
-- (#1 should have 30 rentals and #5 should have 28 rentals)
SELECT COUNT (title)
        , inventory.film_id
        , film_category.category_id
        , film.title
FROM inventory
INNER JOIN rental
        ON rental.inventory_id = inventory.inventory_id
INNER JOIN film
        ON film.film_id = inventory.film_id
INNER JOIN film_category
        ON film.film_id = film_category.film_id        
WHERE film_category.category_id = 1
GROUP BY inventory.film_id
        , film_category.category_id
        , film.title
ORDER BY count DESC LIMIT 5;

-- 19. The top 10 actors ranked by number of rentals of films starring that actor 
-- (#1 should be “GINA DEGENERES�? with 753 rentals and #10 should be “SEAN GUINESS�? with 599 rentals)
SELECT COUNT (film.title)
        , SUM (actor.actor_id) as top_rental_star
        , inventory.film_id
        , actor.first_name
        , actor.last_name
FROM inventory
INNER JOIN rental
        ON rental.inventory_id = inventory.inventory_id
INNER JOIN film
        ON film.film_id = inventory.film_id
INNER JOIN film_actor
        ON film_actor.film_id = film.film_id             
INNER JOIN actor
        ON actor.actor_id = film_actor.actor_id   
GROUP BY inventory.film_id
        , actor.actor_id
        , film.title
        , actor.first_name
        , actor.last_name
ORDER BY top_rental_star DESC LIMIT 10;


-- 20. The top 5 “Comedy�? actors ranked by number of rentals of films in the “Comedy�? category starring that actor 
-- (#1 should have 87 rentals and #5 should have 72 rentals)
SELECT COUNT (title)
        , inventory.film_id
        , film_category.category_id
        , film.title
        , film_actor.film_id
        , film_actor.actor_id
FROM inventory
INNER JOIN rental
        ON rental.inventory_id = inventory.inventory_id
INNER JOIN film
        ON film.film_id = inventory.film_id
INNER JOIN film_category
        ON film.film_id = film_category.film_id
INNER JOIN film_actor
        ON film.film_id = film_actor.film_id        
WHERE film_category.category_id = 5
GROUP BY inventory.film_id
        , film_category.category_id
        , film.title
        , film_actor.film_id
        , film_actor.actor_id
ORDER BY count DESC LIMIT 5;