SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = 'wine_inventory';

DROP DATABASE IF EXISTS wine_inventory;

CREATE DATABASE wine_inventory;
