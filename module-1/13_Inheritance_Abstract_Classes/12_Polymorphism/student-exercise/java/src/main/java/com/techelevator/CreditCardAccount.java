package com.techelevator;

public class CreditCardAccount implements Accountable
{	
	//properties
	private String accountHolder;
	private String accountNumber;
	private int debt;
	
	//constructor creates an instance of the object
	public CreditCardAccount(String accountHolder, String accountNumber)
	{
		//references properties of(this credit card = value of this credit card
		this.accountHolder = accountHolder;
		this.accountNumber = accountNumber;
		debt = 0;
	}
	
	//public methods that return our private properties
	//a.k.a. getters
	public String getAccountHolder()
	{
		return accountHolder;
	}
		
	public String getAccountNumber()
	{
		return accountNumber;
	}
	
	public int getDebt()
	{
		return debt;
	}
	
	//method defines an action the object can do
	public int getBalance()
	{
		return -debt;
	}
	
	public int pay(int amountToPay)
	{
		debt = getDebt() - amountToPay;
		
		return debt;
	}
	
	public int charge(int amountToCharge)
	{
		debt = getDebt() +  amountToCharge;
		
		return debt;
	}
	
	
}

