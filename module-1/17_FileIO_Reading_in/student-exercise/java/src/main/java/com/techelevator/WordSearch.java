package com.techelevator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class WordSearch 
{

	public static void main(String[] args) 
	{
		/*
		 * 1 - specify file path (create a File) // prompt the user for the file name
		 * 2 -create a Scanner to read the FileStream
		 * 3 - loop through the stream - scanner.nextLine() or scanner.next()
		 * 4 - do something with the data on each line
		 */
		Scanner userInput = new Scanner(System.in);                                               
		System.out.println("What is the file to be searched?");
		String path = userInput.nextLine();
		//prompt for search term/phrase
		
		System.out.println("What term or phrase would you like to search for?");
		String search = userInput.nextLine();
		// prompt to see if search is case sensitive
		System.out.println("Should the search word be case sensitive? (Y/N) ");
		String caseSensitive = userInput.nextLine();
		
		boolean isCaseSensitive = false;
		if (caseSensitive.contentEquals("Y"))
			{
			isCaseSensitive = true;
			}
				
		
		File inPutFile = new File(path);
		
		if(inPutFile.exists())
		{

			searchFile(inPutFile, search, isCaseSensitive);
		}
		else 
		{
			System.out.println("The File" + inPutFile + "Does Not Exist");	
		
		}
	}	
		public static void searchFile(File inPutFile, String searchTerm, boolean isCaseSensitive)
		{
			
			try 
			{
			@SuppressWarnings("resource")
			Scanner inPutScanner = new Scanner(inPutFile.getAbsoluteFile());
			int lineCount = 1;
			String line = "";
			while(inPutScanner.hasNextLine())
			{
				
				lineCount++;
				
				line = inPutScanner.nextLine();
				// see if the searchTerm is in this line
				if (line.contains(searchTerm))
				{
				System.out.println(lineCount+ " " + line);
				}
				//if yes
					// print this line number and the line
			}
		
		}
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		}
		
		}

}
