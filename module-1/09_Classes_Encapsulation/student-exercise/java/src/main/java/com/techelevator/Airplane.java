package com.techelevator;

public class Airplane 
{
	private String planeNumber;
	private int totalFirstClassSeats;
	private int bookedFirstClassSeats;
	private int totalCoachSeats;
	private int bookedCoachSeats;
	
		// getters return the value property
	
		public String getPlaneNumber()
		{
		return planeNumber;
		}
		public int getTotalFirstClassSeats()
		{
		return totalFirstClassSeats;
		}
		public int getBookedFirstClassSeats()
		{
			return bookedFirstClassSeats;
		}
		public int getTotalCoachSeats()
		{
			return totalCoachSeats;
		}
		public int getBookedCoachSeats()
		{
			return bookedCoachSeats;
		}
	
		// derived property still a getter but based on our input.
		
		public int getAvailableFirstClassSeats()
		{
			return (totalFirstClassSeats - bookedFirstClassSeats);
		}
		public int getAvailableCoachSeats()
		{
			return (totalCoachSeats - bookedCoachSeats);
		}
		
		// constructor - GIVE US CONTROL
				//over HOW to create the object
				//- this is called on -
		
		public Airplane(String thePlaneNumber, int theTotalFirstClassSeats, int theTotalCoachSeats)
		{
			planeNumber = thePlaneNumber;
			totalFirstClassSeats = theTotalFirstClassSeats;
			totalCoachSeats = theTotalCoachSeats;
		}
		
		// method to book seats
		
		public boolean reserveSeats(boolean forFirstClass, int totalNumberOfSeats)
				
		{		
			if	(forFirstClass && (totalNumberOfSeats) <= (getAvailableFirstClassSeats()))
			{	
				bookedFirstClassSeats = (bookedFirstClassSeats + totalNumberOfSeats);
				return true;
			}	
			
			if (!forFirstClass && (totalNumberOfSeats) <= (getAvailableCoachSeats()))
			{		
				bookedCoachSeats = (bookedCoachSeats + totalNumberOfSeats);
				return true;
			}		
												
			return false;
		}		
}