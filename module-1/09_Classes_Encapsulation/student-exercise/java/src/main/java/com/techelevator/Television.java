package com.techelevator;

public class Television 
{

	private boolean isOn = false;
	private int currentChannel = 3;
	private int currentVolume = 2;
	
	{
		//this.isOn = isOn;
		//this.currentChannel = currentChannel;
		//this.currentVolumne = currentVolumne;
	
	}
	 
	//getters
	public boolean isOn()
	{
		return isOn;
	}
	public int getCurrentChannel()
	{
		return currentChannel;
	}
	public int getCurrentVolume()
	{
		return currentVolume;
	}
	
	
   
	public void turnOff()
	{
		isOn = false;
	}
	public void turnOn ()
	{
		isOn = true;
		currentChannel = 3;
		currentVolume = 2;
	}

    //void changeChannel(int newChannel)
	public void changeChannel(int newChannel)
	{
		if (newChannel >= 3 && newChannel<= 18 && isOn)
		{
			currentChannel = newChannel;
		}
		
	}
	
	//void channelUp()
    public void channelUp()
    { 
    	if (isOn && currentChannel == 18)
    	{
    		currentChannel = 3;
    	}
    	else if (isOn)
    	{
    		currentChannel = currentChannel +1;
    	}
    }	
    	//void channelDown()
	public void channelDown()
    { 
    	if (isOn && currentChannel == 3)
    	{
    		currentChannel = 18;
    	}
    	else if (isOn)
    	{
    		currentChannel = currentChannel -1;
    	}
    
    }
    //void raiseVolume()
    
    public void raiseVolume()
    {
    	if (isOn && currentVolume < 10)
    	{
    		currentVolume = currentVolume + 1;
    	}
    }
    //void lowerVolume()
    public void lowerVolume()
    {
    	if (isOn && currentVolume > 0)
    	{
    		currentVolume = currentVolume - 1;
    	}
    }
    

}
