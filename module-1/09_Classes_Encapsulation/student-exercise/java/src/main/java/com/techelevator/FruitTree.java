package com.techelevator;

public class FruitTree 
{
	private String typeOfFruit;
	private int piecesOfFruit;
	
	public FruitTree (String typeOfFruit, int piecesOfFruit)
	{
		this.typeOfFruit = typeOfFruit;
		this.piecesOfFruit = piecesOfFruit;
	}
		//getters
	
		public String getTypeOfFruit()
		{
		 return typeOfFruit;
		}
		public int getPiecesOfFruitLeft()
		{
		return piecesOfFruit;
		}
		public boolean pickFruit(int numberOfPiecesOfFruit)
		{	
		if (numberOfPiecesOfFruit <= piecesOfFruit)
		
		{	
			piecesOfFruit = piecesOfFruit - numberOfPiecesOfFruit;
			
			return true;
		}
		else		
		
			return false;
			
	}
}
