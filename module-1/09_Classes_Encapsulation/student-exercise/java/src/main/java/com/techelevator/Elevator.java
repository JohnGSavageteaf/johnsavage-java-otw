package com.techelevator;

public class Elevator 
{
	private boolean isDoorOpen = false;
	private int currentFloor;
	private int numberOfFloors;
	

	//Constructor
	public Elevator (int numberOfLevels)
	{
		currentFloor = 1;
		numberOfFloors = numberOfLevels;
	}
	
	
	//getters
	public boolean isDoorOpen()
	{
		return isDoorOpen;
	}
	public int getCurrentFloor()
	{
		return currentFloor;
	}
	public int getNumberOfFloors()
	{
		return numberOfFloors;
	}
	
	
   
	public void closeDoor()
	{
		isDoorOpen = false;
	}
	public void openDoor ()
	{
		isDoorOpen = true;
	}

    
	
	public void goUp(int desiredFloor)
	{
		if (!isDoorOpen && desiredFloor > currentFloor)
		{
			if (desiredFloor > numberOfFloors)
			{
				currentFloor = numberOfFloors;
			}
			else
			{	
				currentFloor = desiredFloor;
			}
		}
		
	}
	public void goDown(int desiredFloor)
	{
		if (!isDoorOpen && desiredFloor < currentFloor)
		{	
			if (desiredFloor > 1)	
			{	
			currentFloor = desiredFloor;	
			}
			else
			{
				currentFloor = 1;
			}
		}
	} 	
	
}
