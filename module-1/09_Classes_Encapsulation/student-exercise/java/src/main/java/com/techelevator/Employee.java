package com.techelevator;

public class Employee 
{
	private int employeeId;
	private String firstName;
	private String lastName;
	private String department;
	private double annualSalary;

	
// getters return the value property
		// that are stored in the private variables
		// these are the functions
	
		public int getEmployeeId()
		{
			return employeeId;
		}
		public String getFirstName()
		{
			return firstName;
		}
		public String getLastName()
		{
			return lastName;		
		}
		public String getDepartment()
		{
			return department;
		}
		public double getAnnualSalary()
		{
			return annualSalary;
		}
		
		// derived property still a getter but based on our input.
		public String getFullName()
		{
			return (lastName + ", " + firstName);
		}

		
		// setters change or set the value of private variables
		public void setLastName(String newLastName)
		{
		  lastName = newLastName;
		}
		public void setDepartment(String newDepartment)
		{
			department = newDepartment;
		}
		
		
		// constructor - GIVE US CONTROL
		//over HOW to create the object
		//- this is called on -> new Employee()
		public Employee(int theEmployeeId, String theFirstName, String theLastName, double theAnnualSalary)
		{
			employeeId = theEmployeeId;
			firstName = theFirstName;
			lastName = theLastName;
			annualSalary = theAnnualSalary;
		}
		
		// method to raise salary
		
		public void raiseSalary(double percent)
		{
			annualSalary = (annualSalary + (annualSalary * percent/100));
			
		}

	
}
