package com.techelevator.cardexample;

public class MainProgram
{

	public static void main(String[] args)
	{
			// creating an object
			// new Keyword calls the constructor - no return type, just builds
		// example  String name = new String("Gregor");
		
		Card myCard = new Card("Spades", 12); 
		
		//myCard.setSuit("Spades"); 
		
		
		System.out.println(myCard.getSuit());
		System.out.println("Face Value: " + myCard.getFaceValue());
		System.out.println("Value: " + myCard.getValue());
		System.out.println("FaceUp: " + myCard.isFaceUp());
		System.out.println("Color: " + myCard.getColor());
		
		System.out.println();
		System.out.println(myCard.getCardInfo());
	
		System.out.println();
		System.out.println("Flipping card");
		myCard.flip();
			
		System.out.println();
		System.out.println(myCard.getCardInfo());
		
		System.out.println();
		System.out.println("Flipping card");
		myCard.flip();
	}
}
