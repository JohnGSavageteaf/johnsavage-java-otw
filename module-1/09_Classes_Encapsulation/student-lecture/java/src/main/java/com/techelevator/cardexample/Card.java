package com.techelevator.cardexample;

import java.util.HashMap;
import java.util.Map;

public class Card 
{
		//private map just another variable
	private Map<Integer, String> valueMap = new HashMap<Integer, String>()
	{
		{
			put(2, "2");
			put(3, "3");
			put(4, "4");
			put(5, "5");
			put(6, "6");
			put(7, "7");
			put(8, "8");
			put(9, "9");
			put(10, "10");
			put(11, "J");
			put(12, "Q");
			put(13, "K");
			put(14, "A");
				
		}
	};
	
	// what does it know
	// should always be private
	
	private String suit;
	private int value;
	private boolean isFaceUp;
	
	// getters return the value property
	// that are stored in the private variables
	// these are the functions
	public String getSuit()
	{
		return suit;
	}
	public int getValue()
	{
		return value;
	}
	
	public boolean isFaceUp()
	{
		return isFaceUp;
	}
		
	// derived property still a getter but based on our input.
	//does NOT have a backing variable
	//the return is calculated based on other variables called "ENCAPSULATION"
	
		public String getFaceValue()
		{
			return valueMap.get(value);
		}
		// below is replaced by a value map above:
	//{
		//if(value < 10)
		//{
		//return "" + value;
		//}
		
		//else
		//{
		//return "face card";

		public String getColor()
		{
			if (suit.equals("Spades") || suit.equals ("Clubs"))
			{
				return "Black";
			}
			else
			{ 
				return "Red";
			}
		
		}
		public String getCardInfo()
		{
			if (isFaceUp)
			{
				return getFaceValue() + " " + suit +  "(" + getColor() + ")";
			}	
			else
			{
				return "##";
			}
		}
		

		
	
	
	
	
	// setters change or set the value of private variables
	public void setSuit(String newSuit)
	{
	  suit = newSuit;
	}
	
	// constructor - GIVE US CONTROL
	//over HOW to create the object
	//- this is called on -> new Cerd()
	public Card(String theSuit, int theValue)
	{
		suit = theSuit;
		value = theValue;
	
		isFaceUp = false; // default
		
	}
	
	public void flip()

	{
		isFaceUp = !isFaceUp;
	}
	
}
