package com.techelevator;

/*
Linear Convert

Write a program that converts meters to feet and vice-versa.

The foot to meter conversion formula is:
m = f * 0.3048

The meter to foot conversion formula is:
f = m * 3.2808399

Write a command line program which prompts a user to enter a length, 
and whether the measurement is in (m)eters or (f)eet. 
Convert the length to the opposite measurement, 
and display the old and new measurements to the console.
Please enter the length: 58
Is the measurement in (m)eter, or (f)eet? f
58f is 17m.
*/
/*
 * public static void main(String[] args) 
	{
		
			Scanner scanner = new Scanner(System.in);
			
			System.out.print("Please enter your weight: ");
			String earthWeight = scanner.nextLine();
			double weight = Double.parseDouble(earthWeight);
	
			System.out.println("Your Mars weight is: " + mWeight(weight));
			
	}		
			public static Double mWeight (double weight)
			{	
			
			double calculateMarsWeight = (weight * 0.378);
			return calculateMarsWeight;
			
			
			}
 */

import java.util.Scanner;

public class LinearConvert 
{
	// where my program starts
	// entry point to the application

	public static void main(String[] args) 
	
	{
		//Streams
				// System.out output stream - to console
				// System.in input stream - from console (raw data)		
				// in order to read from System.in we need a Scanner
		Scanner scanner = new Scanner(System.in);
		// get the length
		System.out.print("Please enter a length ");
		
		double lengthEnter = scanner.nextDouble();
		scanner.nextLine();
		// get the scale
		System.out.print("Is the length in (m)eters or (f)eet ");		
		
		String metersOrFeet = scanner.nextLine();
		
		System.out.println(lengthEnter + metersOrFeet + " is " + lengthConverter(lengthEnter, metersOrFeet) + (metersOrFeet.startsWith("m") ? " f" : " m"));
	
		scanner.close();
	
	
	}	
	public static double lengthConverter(double lengthFinder, String metersOrFeet)
	{
		if(metersOrFeet.startsWith("m"))
		{
			double resultFeet = (lengthFinder *  3.2808399);
			return resultFeet;
			
		}
		else
		{
			double resultMeters = (lengthFinder * 0.3048 );
			return resultMeters;
			
		}

	
	}
	
}
