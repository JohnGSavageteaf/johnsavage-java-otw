package com.techelevator;

/*Fibonacci

The Fibonacci numbers are the integers in the following sequence:
0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, ...

By definition, the first two numbers in the Fibonacci sequence are 0 and 1, and each subsequent number is the sum of the previous two.

Write a command line program which prompts the user for an integer value and display the Fibonacci sequence leading up to that number.
Please enter the Fibonacci number: 25

0, 1, 1, 2, 3, 5, 8, 13, 21
*/
import java.util.Scanner;

public class Fibonacci 
{

	public static void main(String[] args) 
	{

		int maxFibonacciNumber, num1 = 0, num2 = 1;
		System.out.print("Type any number: ");
		Scanner scanner = new Scanner(System.in);
		maxFibonacciNumber = scanner.nextInt();
		
		scanner.close();
		System.out.print("Fibonacci Series of "+maxFibonacciNumber+" numbers:");
		
		int i=1;
		while(maxFibonacciNumber >= num1)
		{
			System.out.println(num1+" ");
			int sumOfPreviousTwo = num1 + num2;
			num1 = num2;
			num2 = sumOfPreviousTwo;
			i++;
			
		}
		
	}

}
