package com.techelevator;

import java.util.Scanner;

public class TempConvert 

/*Temperature Convert

The Fahrenheit to Celsius conversion formula is:
Tc = (Tf - 32) / 1.8

where 'Tc' is the temperature in Celsius, and 'Tf' is the temperature in Fahrenheit.

The Celsius to Fahrenheit conversion formula is:
Tf = Tc * 1.8 + 32

Write a command line program which prompts a user to enter a temperature, and whether it's in degrees (C)elsius or (F)ahrenheit. Convert the temperature to the opposite degrees, and display the old and new temperatures to the console.
Please enter the temperature: 58
Is the temperature in (C)elsius, or (F)ahrenheit? F
58F is 14C.
*/

{
	// where my program starts
	// entry point to the application
	public static void main(String[] args) 
	{
		//Streams
				// System.out output stream - to console
				// System.in input stream - from console (raw data)		
				// in order to read from System.in we need a Scanner
		Scanner scanner = new Scanner(System.in);
		// get temperature
		System.out.print("Please enter the temperature: ");
		
		double tempEnter = scanner.nextInt();
		scanner.nextLine();
		
		// get scale
		System.out.print("Is the temperature in (C)elsius or (F)ahrenheit?");
			
		// call the function
		String fahrenheitOrCelsius = scanner.nextLine();
		// displaying output to the user
		System.out.println(tempEnter + fahrenheitOrCelsius + " is " + tempConversion(tempEnter, fahrenheitOrCelsius) + (fahrenheitOrCelsius.startsWith("C") ? "F" : "C"));		
		
		scanner.close();
					
	}
	
	public static double tempConversion(double tempEnter, String fahrenheitOrCelsius)
	{
		
		
		if(fahrenheitOrCelsius.startsWith("F"))
		{
			
			double resultCelsius = ((tempEnter * 1.8) + 32.0);
			return resultCelsius;
		}
		else
		{	
			double resultFahrenheit = ((tempEnter - 32) / 1.8);
			return resultFahrenheit;
		}
		
		
/*{
		if(metersOrFeet.startsWith("m"))
		{
			double resultFeet = (lengthFinder *  3.2808399);
			return resultFeet;
			
		}
		else
		{
			double resultMeters = (lengthFinder * 0.3048 );
			return resultMeters;
			
		}*/		
	
	
	}	
}

		