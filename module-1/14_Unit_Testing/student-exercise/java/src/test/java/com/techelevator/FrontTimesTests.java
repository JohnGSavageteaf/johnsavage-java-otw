package com.techelevator;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FrontTimesTests 

{  
	private FrontTimes firstCharacters; 

	
	@Before //will happen BEFORE EVERY test
	public void setup()  
	{
		// this is a global arrange block
		firstCharacters = new FrontTimes();	 
	}

	@Test
	public void First_Three_Characters_Times_2() 
	
	{
		//arrange
		String inputString = "Chocolate";
		int n = 2;
		String expected = "ChoCho";
		
		//act
		
		String actual = firstCharacters.generateString(inputString, n);
					  
		//assert
		assertEquals("TheFirstThreeTwice", expected, actual);

	} 
	
	@Test
	public void First_Three_Characters_Times_3() 
	
	{
		//arrange
		String inputString = "Chocolate";
		int n = 3;
		String expected = "ChoChoCho";
		
		//act
		
		String actual = firstCharacters.generateString(inputString, n);
					  
		//assert
		assertEquals("TheFirstThreeThrice", expected, actual);

	} 
	@Test
	public void First_Three_Characters_Times_4() 
	
	{
		//arrange
		String inputString = "Abc";
		int n = 4;
		String expected = "AbcAbcAbcAbc";
		
		//act
		
		String actual = firstCharacters.generateString(inputString, n);
					  
		//assert
		assertEquals("TheFirstThreeFour", expected, actual);

	} 
	
	@Test
	public void First_Two_Characters_Times_4() 
	
	{
		//arrange
		String inputString = "Ab";
		int n = 4;
		String expected = "AbAbAbAb";
		
		//act
		
		String actual = firstCharacters.generateString(inputString, n);
					  
		//assert
		assertEquals("TheFirstTwoFour", expected, actual);

	} 
	
	@Test
	public void First_One_Characters_Times_4() 
	
	{
		//arrange
		String inputString = "A";
		int n = 4;
		String expected = "AAAA";
		
		//act
		
		String actual = firstCharacters.generateString(inputString, n);
					  
		//assert
		assertEquals("TheFirstThreeTwice", expected, actual);
	}
		@Test
		public void First_Empty_Characters_Times_4() 
		
		{
			//arrange
			String inputString = "";
			int n = 4;
			String expected = "";
			
			//act
			
			String actual = firstCharacters.generateString(inputString, n);
						  
			//assert
			assertEquals("NoCharactersFourTimes", expected, actual);

		}
		@Test
		public void First_Three_Characters_Times_0() 
		
		{
			//arrange
			String inputString = "Abc";
			int n = 0;
			String expected = "";
			
			//act
			
			String actual = firstCharacters.generateString(inputString, n);
						  
			//assert
			assertEquals("TheFirstThreeTimesZero", expected, actual);
		}
	
	
	
	/*
	@Test
	public void frontTimes() {
		assertEquals("Input: frontTimes(\"Chocolate\", 2)", "ChoCho", exercises.frontTimes("Chocolate", 2));
		assertEquals("Input: frontTimes(\"Chocolate\", 3)", "ChoChoCho", exercises.frontTimes("Chocolate", 3));
		assertEquals("Input: frontTimes(\"Abc\", 3)", "AbcAbcAbc", exercises.frontTimes("Abc", 3));
		assertEquals("Input: frontTimes(\"Ab\", 4)", "AbAbAbAb", exercises.frontTimes("Ab", 4));
		assertEquals("Input: frontTimes(\"A\", 4)", "AAAA", exercises.frontTimes("A", 4));
		assertEquals("Input: frontTimes(\"\", 4)", "", exercises.frontTimes("", 4));
		assertEquals("Input: frontTimes(\"Abc\", 0)", "", exercises.frontTimes("Abc", 0));
	} */
}
