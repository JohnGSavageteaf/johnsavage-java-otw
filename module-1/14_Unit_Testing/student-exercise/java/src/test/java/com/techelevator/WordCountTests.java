package com.techelevator;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
//import static org.hamcrest.Matchers.equalTo;
//import static org.hamcrest.Matchers.hasEntry;
import static org.junit.Assert.assertThat;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class WordCountTests 
{ 

	private WordCount keyDictionary; 
	private Map<String, Integer> output;
	
	@Before //will happen BEFORE EVERY test
	public void setup() 
	{
		// this is a global arrange block
		keyDictionary = new WordCount();	 
	}

	@Test
	public void Dictionary_Map_To_String_a() 
	
	{
		//arrange inputs
		String[] inputString = {"a"};
		
		int expectedSize = 1;
		
		//act
		
		output = keyDictionary.getCount(inputString);
					  
		//assert
		assertThat("a_occurs_1Time", output.size(), equalTo(expectedSize)); 
 
	}
	@Test
	public void Dictionary_Map_To_String_FiddleDeeDee() 
	
	{
		//arrange inputs
		String[] inputString = {"Fiddle", "Dee", "Dee"};
		
		int expectedSize = 2;
		
		//act
		
		output = keyDictionary.getCount(inputString);
					  
		//assert
		assertThat("a_occurs_1Time", output.size(), equalTo(expectedSize)); 
 
	}
	
	@Test
	public void Dictionary_Map_To_String_() 
	
	{
		//arrange inputs
		String[] inputString = {"Oh", "woeful", "oh", "woeful", "woeful", "woeful", "day"};
		
		int expectedSize = 4;
		
		//act
		
		output = keyDictionary.getCount(inputString);
					  
		//assert
		assertThat("a_occurs_1Time", output.size(), equalTo(expectedSize)); 
 
	}
	
/*	@Test
	public void exercise06_wordCount() {
		Map<String, Integer> output = exercises.wordCount(new String[] { "a", "b", "a", "c", "b" });
		assertThat("wordCount([\"a\", \"b\", \"a\", \"c\", \"b\"])", output.size(), equalTo(3));
		assertThat("wordCount([\"a\", \"b\", \"a\", \"c\", \"b\"])", output, hasEntry("a", 2));
		assertThat("wordCount([\"a\", \"b\", \"a\", \"c\", \"b\"])", output, hasEntry("b", 2));
		assertThat("wordCount([\"a\", \"b\", \"a\", \"c\", \"b\"])", output, hasEntry("c", 1));

		output = exercises.wordCount(new String[] { "c", "b", "a" });
		assertThat("wordCount([\"c\", \"b\", \"a\"])", output.size(), equalTo(3));
		assertThat("wordCount([\"c\", \"b\", \"a\"])", output, hasEntry("a", 1));
		assertThat("wordCount([\"c\", \"b\", \"a\"])", output, hasEntry("b", 1));
		assertThat("wordCount([\"c\", \"b\", \"a\"])", output, hasEntry("c", 1));

		output = exercises.wordCount(new String[] { });
		assertThat("wordCount([])", output.size(), equalTo(0));
		
		output = exercises.wordCount(new String[] { "ba", "ba", "black", "sheep" });
		assertThat("wordCount([\"ba\", \"ba\", \"black\", \"sheep\"])", output.size(), equalTo(3));
		assertThat("wordCount([\"ba\", \"ba\", \"black\", \"sheep\"])", output, hasEntry("ba", 2));
		assertThat("wordCount([\"ba\", \"ba\", \"black\", \"sheep\"])", output, hasEntry("black", 1));
		assertThat("wordCount([\"ba\", \"ba\", \"black\", \"sheep\"])", output, hasEntry("sheep", 1));

		output = exercises.wordCount(new String[] { "ba", "ba", "black", "sheep", "ba", "ba", "black", "sheep" });
		assertThat("wordCount([\"ba\", \"ba\", \"black\", \"sheep\", \"ba\", \"ba\", \"black\", \"sheep\"])", output.size(), equalTo(3));
		assertThat("wordCount([\"ba\", \"ba\", \"black\", \"sheep\", \"ba\", \"ba\", \"black\", \"sheep\"])", output, hasEntry("ba", 4));
		assertThat("wordCount([\"ba\", \"ba\", \"black\", \"sheep\", \"ba\", \"ba\", \"black\", \"sheep\"])", output, hasEntry("black", 2));
		assertThat("wordCount([\"ba\", \"ba\", \"black\", \"sheep\", \"ba\", \"ba\", \"black\", \"sheep\"])", output, hasEntry("sheep", 2));
		
		output = exercises.wordCount(new String[] { "apple", "apple", "banana", "apple", "carrot", "banana", "dill", "dill", "banana", "apple" });
		assertThat("wordCount([\"apple\", \"apple\", \"banana\", \"apple\", \"carrot\", \"banana\", \"dill\", \"dill\", \"banana\", \"apple\"])", output.size(), equalTo(4));
		assertThat("wordCount([\"apple\", \"apple\", \"banana\", \"apple\", \"carrot\", \"banana\", \"dill\", \"dill\", \"banana\", \"apple\"])", output, hasEntry("apple", 4));
		assertThat("wordCount([\"apple\", \"apple\", \"banana\", \"apple\", \"carrot\", \"banana\", \"dill\", \"dill\", \"banana\", \"apple\"])", output, hasEntry("banana", 3));
		assertThat("wordCount([\"apple\", \"apple\", \"banana\", \"apple\", \"carrot\", \"banana\", \"dill\", \"dill\", \"banana\", \"apple\"])", output, hasEntry("carrot", 1));
		assertThat("wordCount([\"apple\", \"apple\", \"banana\", \"apple\", \"carrot\", \"banana\", \"dill\", \"dill\", \"banana\", \"apple\"])", output, hasEntry("dill", 2));

		output = exercises.wordCount(new String[] { "apple", "apple", "apple", "apple", "apple", "apple" });
		assertThat("wordCount([\"apple\", \"apple\", \"apple\", \"apple\", \"apple\", \"apple\"])", output.size(), equalTo(1));
		assertThat("wordCount([\"apple\", \"apple\", \"apple\", \"apple\", \"apple\", \"apple\"])", output, hasEntry("apple", 6));

	} */

}
