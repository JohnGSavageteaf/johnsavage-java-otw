package com.techelevator;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test; 

public class AnimalGroupNameTests    
{
	private AnimalGroupName animals; 

	
	@Before //will happen BEFORE EVERY test
	public void setup() 
	{
		// this is a global arrange block
		animals = new AnimalGroupName();	 
	}

	@Test
	public void Herd_Names_For_Animal_Giraffe() 
	
	{
		//arrange inputs
		String myGiraffe = "giraffe";
		
		//act
		
		String actual = animals.getHerd(myGiraffe);
					  
		//assert
		assertEquals("The_Herd_Name_For_giraffe", actual, "Tower");
 
	}
	@Test
	public void Herd_Names_For_Animal_Unknown() 
	
	{
		//arrange inputs
		String myUnknown = "";
		
		//act
		
		String actual = animals.getHerd(myUnknown);
					  
		//assert
		assertEquals("The_Herd_Name_For_Blank", actual, "unknown");
 
	}
	@Test
	public void Herd_Names_For_Animal_BigGiraffe() 
	
	{
		//arrange inputs
		String myGiraffe = "Giraffe";
		
		//act
		
		String actual = animals.getHerd(myGiraffe);
					  
		//assert
		assertEquals("The_Herd_Name_For_Giraffe", actual, "Tower");
 
	}
	@Test
	public void Herd_Names_For_Animal_Walrus() 
	
	{
		//arrange inputs
		String myWalrus = "Walrus";
		
		//act
		
		String actual = animals.getHerd(myWalrus);
					  
		//assert
		assertEquals("The_Herd_Name_For_Walrus", actual, "unknown");
 
	}
	@Test
	public void Herd_Names_For_Animal_elephants() 
	
	{
		//arrange inputs
		String myelephants = "elephants";
		
		//act
		
		String actual = animals.getHerd(myelephants);
					  
		//assert
		assertEquals("The_Herd_Name_For_elephants", actual, "unknown");
 
	}
	@Test
	public void Herd_Names_For_Animal_CrocodilE() 
	
	{
		//arrange inputs
		String myCrocodilE = "CrocodilE";
		
		//act
		
		String actual = animals.getHerd(myCrocodilE);
					  
		//assert
		assertEquals("The_Herd_Name_For_CrocodilE", actual, "Float");
 
	}
}
