package com.techelevator;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StringBitsTests  
{

		private StringBits bitsOfString;  
		
		@Before //will happen BEFORE EVERY test
		public void setup()
		{
			bitsOfString = new StringBits(); 	
		} 
	@Test
	public void new_String_Made_Of_Everyother_Character()
	{
		//arrange
		String HelloString = ("Hello");
		 
		//act
		String actual = bitsOfString.getBits(HelloString);
		
		//assert
		Assert.assertEquals(actual, actual);
	}
	@Test
	public void Hi_new_String_Made_Of_Everyother_Character()
	{
		//arrange
		String HelloString = ("Hi");
		 
		//act
		String actual = bitsOfString.getBits(HelloString); 
		
		//assert
		Assert.assertEquals(actual, actual);
	}
	@Test
	public void Happy_new_String_Made_Of_Everyother_Character()
	{
		//arrange
		String HelloString = ("Hxaxpxpxyx");
		 
		//act
		String actual = bitsOfString.getBits(HelloString);
		
		//assert
		Assert.assertEquals(actual, actual);
	} 
/*	@Test
	public void stringBits() {
		assertEquals("Input: stringBits(\"Hello\")", "Hlo", exercises.stringBits("Hello"));
		assertEquals("Input: stringBits(\"Hi\")", "H", exercises.stringBits("Hi"));
		assertEquals("Input: stringBits(\"Heeololeo\")", "Hello", exercises.stringBits("Heeololeo"));
		assertEquals("Input: stringBits(\"HiHiHi\")", "HHH", exercises.stringBits("HiHiHi"));
		assertEquals("Input: stringBits(\"\")", "", exercises.stringBits(""));
		assertEquals("Input: stringBits(\"Greetings\")", "Getns", exercises.stringBits("Greetings"));
		assertEquals("Input: stringBits(\"Chocoate\")", "Coot", exercises.stringBits("Chocoate"));
		assertEquals("Input: stringBits(\"pi\")", "p", exercises.stringBits("pi"));
		assertEquals("Input: stringBits(\"Hello Kitten\")", "HloKte", exercises.stringBits("Hello Kitten"));
		assertEquals("Input: stringBits(\"hxaxpxpxy\")", "happy", exercises.stringBits("hxaxpxpxy"));
	}  */

}
