package com.techelevator;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
 
public class Less20Tests 
{
	private Less20 twenty;  
	
	@Before //will happen BEFORE EVERY test
	public void setup()
	{
		twenty = new Less20(); 	
	} 
	@Test
	public void remainderOfTwenty_ShouldBeTrue_ForValues_OneLessThan_Twenty()
	{
		//arrange
		int oneLessThanTwenty = 19;
		 
		//act
		boolean actual = twenty.isLessThanMultipleOf20(oneLessThanTwenty);
		
		//assert
		Assert.assertTrue("19_Should_Be_OneLessThanTwenty" , actual);
	}
	@Test
	public void remainderOfTwenty_ShouldBeTrue_ForValues_TwoLessThan_Twenty()
	{
		//arrange
		int twoLessThanTwenty = 18;
		
		//act
		boolean actual = twenty.isLessThanMultipleOf20(twoLessThanTwenty);
		
		//assert
		Assert.assertTrue("19_Should_Be_OneLessThanTwenty" , actual); 
	}
	@Test
	public void remainderOfValuesEqualToTwenty_ShouldBeFalse()
	{
		//arrange
		int exactlyTwenty = 20;
		
		//act
		boolean actual = twenty.isLessThanMultipleOf20(exactlyTwenty);
		
		//assert
		Assert.assertFalse("20_shouldLeaveARamainderof_Zero" , actual);
	}
	
	@Test
	public void remainderOfTwenty_ShouldBeFalse()
	{
		//arrange
		int exactlyTwenty = 21;
		
		//act
		boolean actual = twenty.isLessThanMultipleOf20(exactlyTwenty);
		
		//assert
		Assert.assertFalse("21_shouldNotLeaveARamainder" , actual);
	}
}
