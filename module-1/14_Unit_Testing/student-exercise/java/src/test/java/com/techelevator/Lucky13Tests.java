package com.techelevator;
 
import static org.junit.Assert.assertEquals;


import org.junit.Before;
import org.junit.Test;
 
public class Lucky13Tests  
{ 
	private Lucky13 lucky; 

	
	@Before //will happen BEFORE EVERY test
	public void setup() 
	{
		// this is a global arrange block
		lucky = new Lucky13();
		
	}

	@Test
	public void You_have_a_Lucky_Array_Test0_2_4() 
	
	{
		//arrange
				
		//act
		
		//boolean actual = lucky.getLucky(null);
		boolean actual = lucky.getLucky(new int[] {0, 2, 4});
		
		//assert
		assertEquals("ThereAreNoOnesOrThrees", true, actual);

	}

	@Test
	public void You_have_an_Unlucky_Array_Test_1_2_3()
	
	{
		//arrange
				
		//act
		boolean actual = lucky.getLucky(new int[] {1, 2, 3}); 
		
		//assert
		assertEquals("Found_A_One_Or_A_Three", false, actual);

	}
	@Test
	public void You_have_an_Unlucky_Array_Test_5_2_3()
	
	{
		//arrange
				
		//act
		boolean actual = lucky.getLucky(new int[] {5, 2, 3});
		
		//assert
		assertEquals("Found_A_One_Or_A_Three", false, actual);

	}
	@Test
	public void You_have_an_Unlucky_Array_Test_13_13_3()
	
	{
		//arrange
				
		//act
		boolean actual = lucky.getLucky(new int[] {13, 13, 3});
		
		//assert
		assertEquals("Found_A_One_Or_A_Three", false, actual);

	}
	@Test
	public void You_have_an_Very__Lucky_Array_Test_13_13_13()
	
	{
		//arrange
				
		//act
		boolean actual = lucky.getLucky(new int[] {13, 13, 13});
		
		//assert
		assertEquals("Found_A_One_Or_A_Three", true, actual);

	}
	 
	
	/*
	 lucky13([0, 2, 4]) → true
	 lucky13([1, 2, 3]) → false
	 lucky13([1, 2, 4]) → false
	 
	@Test
	public void lucky13() {
		//assertEquals("Input: lucky13(new int[]{0, 2, 4})", true,lucky13(new int[] { 0, 2, 4 }));
		//assertEquals("Input: lucky13(new int[]{1, 2, 3})", false,lucky13(new int[] { 1, 2, 3 }));
		//assertEquals("Input: lucky13(new int[]{1, 2, 4})", false, exercises.lucky13(new int[] { 1, 2, 4 }));
		//assertEquals("Input: lucky13(new int[]{5, 2, 3})", false, exercises.lucky13(new int[] { 5, 2, 3 }));

		//assertEquals("Input: lucky13(new int[]{2, 1, 0})", false, exercises.lucky13(new int[] { 2, 1, 0 }));
		//assertEquals("Input: lucky13(new int[]{2, 3, 0})", false, exercises.lucky13(new int[] { 2, 3, 0 }));
		//assertEquals("Input: lucky13(new int[]{2, 4, 1})", false, exercises.lucky13(new int[] { 2, 4, 1 }));
		//assertEquals("Input: lucky13(new int[]{3, 0, 2})", false, exercises.lucky13(new int[] { 3, 0, 2 }));
		//assertEquals("Input: lucky13(new int[]{0, 3, 4})", false, exercises.lucky13(new int[] { 0, 3, 4 }));
	}
*/
}
