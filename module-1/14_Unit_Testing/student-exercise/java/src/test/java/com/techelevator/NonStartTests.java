package com.techelevator;

import static org.junit.Assert.assertEquals; 

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class NonStartTests  
{ 
 
	private NonStart secondLetterStart;  
	
	@Before //will happen BEFORE EVERY test
	public void setup()
	{
		secondLetterStart = new NonStart(); 	
	}
	@Test
	public void newConcentration_Starting_With_Second_Character_Of_Each_HelloThere()
 	{
	//arrange	
		String a = "Hello"; 
		String b = "There";
		
	//act
	String actual = secondLetterStart.getPartialString(a, b);
	
	//assert
	Assert.assertEquals(actual, actual);
 	}

	@Test
	public void newConcentration_Starting_With_Second_Character_Of_Each_JavaCode()
	{
	//arrange	
		String a = "Java"; 
		String b = "Code";
		
	//act
	String actual = secondLetterStart.getPartialString(a, b);
	
	//assert
	Assert.assertEquals(actual, actual);
	}	
	@Test
	public void newConcentration_Starting_With_Second_Character_Of_Each_AJohnCSavage()
	{
	//arrange	
		String a = "AJohn"; 
		String b = "CSavage";
		
	//act
	String actual = secondLetterStart.getPartialString(a, b);
	
	//assert
	Assert.assertEquals(actual, actual);
}	
	
	
	
	
	
	
	
	
	
	/*	@Test
	public void nonStart() {
		assertEquals("Input: nonStart(\"Hello\", \"There\")", "ellohere", exercises.nonStart("Hello", "There"));
		assertEquals("Input: nonStart(\"java\", \"code\")", "avaode", exercises.nonStart("java", "code"));
		assertEquals("Input: nonStart(\"shotl\", \"java\")", "hotlava", exercises.nonStart("shotl", "java"));
		assertEquals("Input: nonStart(\"ab\", \"xy\")", "by", exercises.nonStart("ab", "xy"));
		assertEquals("Input: nonStart(\"ab\", \"x\")", "b", exercises.nonStart("ab", "x"));
		assertEquals("Input: nonStart(\"x\", \"ac\")", "c", exercises.nonStart("x", "ac"));
		assertEquals("Input: nonStart(\"a\", \"x\")", "", exercises.nonStart("a", "x"));
		assertEquals("Input: nonStart(\"kit\", \"kat\")", "itat", exercises.nonStart("kit", "kat"));
		assertEquals("Input: nonStart(\"mart\", \"dart\")", "artart", exercises.nonStart("mart", "dart"));
	}  */
}
