package com.techelevator;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class SameFirstLastTests 
{   
	private SameFirstLast same; 
	private int[] isItTheSame;
	
	@Before //will happen BEFORE EVERY test
	public void setup() 
	{
		// this is a global arrange block
		same = new SameFirstLast();	 
	}

	@Test
	public void The_First_And_Last_Element_Are_Not_Equal() 
	
	{
		//arrange
		//isItTheSame = isItTheSame()[1,2,3])	
		
		//act
		
		boolean actual = same.isItTheSame(new int[] {1, 2, 3});
					  
		//assert
		assertEquals("TheFirstAndLastAreNotTheSame", false, actual);

	}

	@Test
	public void The_First_And_Last_Element_Are_Equal() 
	
	{
		//arrange
		//isItTheSame = isItTheSame()[1,2,3])
		
		//act
		boolean actual = same.isItTheSame(new int[] {1, 2, 3, 1});
		
		//assert
		assertEquals("TheFirstAndLastAreNotTheSame", true, actual);

	}
	@Test
	public void The_First_And_Last_Of_Three_Elements_Are_Equal()  
	
	{
		//arrange
		//isItTheSame = isItTheSame()[1,2,3])
		
		//act
		boolean actual = same.isItTheSame(new int[] {1, 2, 1});
		
		//assert
		assertEquals("TheFirstAndLastAreNotTheSame", true, actual);

	}
	@Test
	public void The_Middle_Three_Elements_Are_Equal() 
	
	{
		//arrange
		//isItTheSame = isItTheSame()[1,2,3])
		
		//act
		boolean actual = same.isItTheSame(new int[] {4, 1, 1, 1, 2}); 
		
		//assert
		assertEquals("TheFirstAndLastAreNotTheSame", false, actual);

	}
	@Test
	public void All_Three_Elements_Are_Equal() 
	
	{
		//arrange
		//isItTheSame = isItTheSame()[1,2,3])
		
		//act
		boolean actual = same.isItTheSame(new int[] {1, 1, 1});
		
		//assert
		assertEquals("TheFirstAndLastAreNotTheSame", true, actual);

	}
	
	
	/*
	 sameFirstLast([1, 2, 3]) → false
	 sameFirstLast([1, 2, 3, 1]) → true
	 sameFirstLast([1, 2, 1]) → true
	 
	@Test
	public void sameFirstLast() {
		assertEquals("Input: sameFirstLast(new int[]{1, 2, 3})", false, exercises.sameFirstLast(new int[] { 1, 2, 3 }));
		assertEquals("Input: sameFirstLast(new int[]{1, 2, 3, 1})", true,
				exercises.sameFirstLast(new int[] { 1, 2, 3, 1 }));
		assertEquals("Input: sameFirstLast(new int[]{1, 2, 1})", true, exercises.sameFirstLast(new int[] { 1, 2, 1 }));
	} */

}
