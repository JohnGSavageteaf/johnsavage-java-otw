package com.techelevator;
 
import static org.junit.Assert.assertEquals;


import org.junit.Before;
import org.junit.Test;
 

public class DateFashioonTests   
{
	private DateFashion fashion;

	
	@Before //will happen BEFORE EVERY test
	public void setup()	
	{
		fashion = new DateFashion();		
	}
	
	@Test
	public void	chanceToGetTable_if_I_am_2_and_Date_Is_6_Zero()
	{ 
		//arrange
		int mine = 2; 
		int date = 6; 
		int getTable = 0; 
		
		//act
		int actual = fashion.getATable(mine, date);
		
		//assert
		assertEquals("Because_I_have_no_fashion", getTable, actual);
	} 
	@Test
	public void	ChanceToGetTable_if_I_am_3_and_Date_Is_7_Maybe()
	{
		//arrange
		int mine = 3;
		int date = 7;
		int getTable = 1;
		 
		//act
		int actual = fashion.getATable(mine, date);
		
		//assert
		assertEquals("Because_I_have_some_fashion", getTable, actual);
	}
	@Test
	public void	ChanceToGetTable_if_I_am_1_and_Date_Is_7_Zero()
	{
		//arrange
		int mine = 1; 
		int date = 7;
		int getTable = 0;
		
		//act
		int actual = fashion.getATable(mine, date);
		
		//assert
		assertEquals("Because_I_am_a_slob", getTable, actual);
	} 
	@Test
	public void	ChanceToGetTable_if_I_am_3_and_Date_Is_9_Yes()
	{
		//arrange
		int mine = 3;
		int date = 9;
		int getTable = 2;
		
		//act
		int actual = fashion.getATable(mine, date);
		
		//assert
		assertEquals("Because_I_have_some_fashion_and_my_date_has_more", getTable, actual);
	} 
	@Test
	public void	ChanceToGetTable_if_I_am_8_and_Date_Is_9_Yes()
	{
		//arrange
		int mine = 8;
		int date = 9;
		int getTable = 2;
		
		//act
		int actual = fashion.getATable(mine, date);
		
		//assert
		assertEquals("Because_my_date_dressed_us", getTable, actual);
	} 
}
