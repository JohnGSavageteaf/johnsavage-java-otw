package com.techelevator;

import static org.junit.Assert.assertArrayEquals;



import org.junit.Before;
import org.junit.Test;
 
public class MaxEnd3Tests 

{

	private MaxEnd3 maximum; 
	
	@Before //will happen BEFORE EVERY test
	public void setup() 
	{
		// this is a global arrange block
		maximum = new MaxEnd3();
		
	}

	@Test
	public void Your_New_Changed_Array_From_Position_Zero() 
	
	{
		//arrange
		int[] expected = {11, 11, 11}; 
		
		//act
		
		int[] actual = maximum.makeArray(new int[] {11, 5, 9});
		 
		//assert
		assertArrayEquals("TheLargestWasFirst", expected, actual);

	} 
	@Test
	public void Your_New_Changed_Array_From_Position_Two() 
	
	{
		//arrange
		int[] expected = {3, 3, 3};
		
		//act
		
		int[] actual = maximum.makeArray(new int[] {2, 11, 3});
		 
		//assert
		assertArrayEquals("TheLargestWasSecond", expected, actual);

	} 
	@Test
	public void Your_New_Changed_Array_From_Position_Three() 
	
	{
		//arrange
		int[] expected = {11, 11, 11};
		
		//act
		
		int[] actual = maximum.makeArray(new int[] {2, 3, 11});
		 
		//assert
		assertArrayEquals("TheLargestWasLast", expected, actual);

	} 

	@Test
	public void Your_New_Changed_Array_From_Position_Unknown() 
	
	{
		//arrange
		int[] expected = {11, 11, 11};
		
		//act
		
		int[] actual = maximum.makeArray(new int[] {9, 11, 11});
		 
		//assert
		assertArrayEquals("TheElevenIsThereTwice", expected, actual);

	} 
	@Test
	public void Your_New_Changed_Array_From_Position_NotMiddle() 
	
	{
		//arrange
		int[] expected = {0, 0, 0};
		
		//act
		
		int[] actual = maximum.makeArray(new int[] {0, 11, 0});
		 
		//assert
		assertArrayEquals("TheElevenIsInTheMiddle", expected, actual);

	} 

	/*
	 maxEnd3([1, 2, 3]) → [3, 3, 3]
	 maxEnd3([11, 5, 9]) → [11, 11, 11]
	 maxEnd3([2, 11, 3]) → [3, 3, 3]
	 
	@Test
	public void maxEnd3() {
		assertArrayEquals("Input: maxEnd3(new int[]{1, 2, 3})", new int[] { 3, 3, 3 },
				exercises.maxEnd3(new int[] { 1, 2, 3 }));
		assertArrayEquals("Input: maxEnd3(new int[]{11, 5, 9})", new int[] { 11, 11, 11 },
				exercises.maxEnd3(new int[] { 11, 5, 9 }));
		assertArrayEquals("Input: maxEnd3(new int[]{2, 11, 3})", new int[] { 3, 3, 3 },
				exercises.maxEnd3(new int[] { 2, 11, 3 }));
	} */
}
