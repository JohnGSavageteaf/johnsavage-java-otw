package com.techelevator;

import org.junit.Test;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;

public class CigarPartyTests   
{ 
	
	private CigarParty party;
	private static final int THIRTY_CIGARS = 30;
	private static final int FORTY_CIGARS = 40;
	//private boolean IS_WEEKEND = true;
	private boolean IS_NOT_WEEKEND = false;
	
	@Before //will happen BEFORE EVERY test
	public void setup() 
	{
		// this is a global arrange block
		party = new CigarParty();
	}
	
	@After // runs AFTER every test
	public void teardown()
	{
		// global cleanup block
		// i.e. undo any changes in the data base
		// delete any files that were created
		 
	}
	
	
	@Test
	public void haveParty_shouldReturnFalse_with30CigarsOnWeekday()
	{
		//arrange
		//don't need to create Cigar Party because it's done in the @Before
		//CigarParty party = new CigarParty();
		//int numberOfCigars = 30;
		//boolean isWeekend = false;
		
		//boolean expected = false; 
		
		
		//act
		// call the function of the object under test
		boolean actual = party.haveParty(THIRTY_CIGARS, IS_NOT_WEEKEND);  
		
		//assert
		Assert.assertFalse("Because_parties_with_<_40_cigars_suck",actual);
		
		
		
	}
	 
	@Test
	public void haveParty_shouldReturnFalse_with39CigarsOnWeekday()
	{
		//arrange
		//CigarParty party = new CigarParty();
		int numberOfCigars = 39;
		boolean isWeekend = false;
		
		//boolean expected = false;
		
		
		//act
		// call the function of the object under test
		boolean actual = party.haveParty(numberOfCigars, isWeekend);
		
		//assert
		Assert.assertFalse("Because_parties_with_<_40_cigars_suck",actual);
		
	}
	
	@Test
	public void haveParty_shouldReturnTrue_with41CigarsOnWeekday()
	{
		//arrange
		//CigarParty party = new CigarParty();
		int numberOfCigars = 41;
		boolean isWeekend = false;
		
		//boolean expected = true;
		
		
		//act
		// call the function of the object under test
		boolean actual = party.haveParty(numberOfCigars, isWeekend);
		
		//assert
		Assert.assertTrue("Because_parties_with_>_40_cigars_rock",actual);
		
	}
	
	@Test
	public void haveParty_shouldReturnTrue_with60CigarsOnWeekday()
	{
		//arrange
		//CigarParty party = new CigarParty();
		int numberOfCigars = 60;
		boolean isWeekend = false;
		
		//boolean expected = true;
		
		
		//act
		// call the function of the object under test
		boolean actual = party.haveParty(numberOfCigars, isWeekend);
		
		//assert
		Assert.assertTrue("Because_parties_with_>_40_cigars_rock",actual);
		
	}
	
	@Test
	public void haveParty_shouldReturnFalse_with61CigarsOnWeekday()
	{
		//arrange
		//CigarParty party = new CigarParty();
		int numberOfCigars = 61;
		boolean isWeekend = false;
		
		//boolean expected = false;
		
		
		//act
		// call the function of the object under test
		boolean actual = party.haveParty(numberOfCigars, isWeekend);
		
		//assert
		Assert.assertFalse("Because_parties_with_>_60_cigars_on_the_weekend_are_too_smokey_and_the_bear_shows_up",actual);
		
	}
	
	@Test
	public void haveParty_shouldReturnTrue_with50CigarsOnWeekday()
	{
		//arrange
		//CigarParty party = new CigarParty();
		int numberOfCigars = 50;
		boolean isWeekend = false;
		
		//boolean expected = true;
		
		
		//act
		// call the function of the object under test
		boolean actual = party.haveParty(numberOfCigars, isWeekend);
		
		//assert
		Assert.assertTrue("Because_parties_with_50_cigars_on_the_weekend_are_right_down_the_middle",actual);
		
	}
	@Test
	public void minimum_numberOfCigars_greaterThanOrEqualTo40()
	{
		//arrange
		//CigarParty party = new CigarParty();
		//int cigars = 40;
		//int minimumCigarCount = 40;
		//boolean haveParty = true;
		
		//act
		// call the function of the object under test
		boolean actual = party.haveParty(FORTY_CIGARS, IS_NOT_WEEKEND);
		
		//assert
		Assert.assertTrue("Because_we_have_the_minimumNumberOfCigars", actual);
		
	}
	
	
	@Test
	public void haveParty_shouldReturnTrue_with70CigarsOnWeekend()
	{
		//arrange
		//CigarParty party = new CigarParty();
		int numberOfCigars = 70;
		boolean isWeekend = true;
		
		//boolean expected = true;
		
		
		//act
		// call the function of the object under test
		boolean actual = party.haveParty(numberOfCigars, isWeekend);
		
		//assert
		Assert.assertTrue("Because_parties_with_70_cigars_on_the_weekend_are_baller!",actual);
		
	}
	@Test
	public void haveParty_shouldReturnTrue_with40CigarsOnWeekend()
	{
		//arrange
		//CigarParty party = new CigarParty();
		int numberOfCigars = 40;
		boolean isWeekend = true;
		
		//boolean expected = true;
		
		
		//act
		// call the function of the object under test
		boolean actual = party.haveParty(numberOfCigars, isWeekend);
		
		//assert
		Assert.assertTrue("Because_parties_with_40_cigars_on_the_weekend_are_cool!",actual);
		
	}
	@Test
	public void haveParty_shouldReturnTrue_with59CigarsOnWeekend()
	{
		//arrange
		//CigarParty party = new CigarParty();
		int numberOfCigars = 59;
		boolean isWeekend = true;
		
		//boolean expected = true;
		
		
		//act
		// call the function of the object under test
		boolean actual = party.haveParty(numberOfCigars, isWeekend);
		
		//assert
		Assert.assertTrue("Because_parties_with_59_cigars_on_the_weekend_are_edgy!",actual);
		
	}
	@Test
	public void haveParty_shouldReturnTrue_with61CigarsOnWeekend()
	{
		//arrange
		//CigarParty party = new CigarParty();
		int numberOfCigars = 61;
		boolean isWeekend = true;
		
		//boolean expected = true;
		
		
		//act
		// call the function of the object under test
		boolean actual = party.haveParty(numberOfCigars, isWeekend);
		
		//assert
		Assert.assertTrue("Because_parties_with_61_cigars_on_the_weekend_are_edgy!",actual);
		
	}
}
