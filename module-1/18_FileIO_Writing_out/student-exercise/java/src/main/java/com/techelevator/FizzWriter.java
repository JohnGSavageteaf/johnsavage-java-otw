package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class FizzWriter 
{

	public static void main(String[] args) 
	{
		File fizzBuzzFile = new File("FizzBuzz.txt");
		try {
			PrintWriter fizzBuzzWriter = new PrintWriter(fizzBuzzFile.getAbsolutePath());

			for (int i = 1; i <= 300; i++) 
			{
				if (i % 3 == 0 && i % 5 == 0) 
				{
					fizzBuzzWriter.println("fizzbuzz");
				} else if (i % 3 == 0 || Integer.toString(i).contains("3")) 
				{
					fizzBuzzWriter.println("fizz");
				} else if (i % 5 == 0 || Integer.toString(i).contains("5")) 
				{
					fizzBuzzWriter.println("buzz");
				} else
					fizzBuzzWriter.println(i);
			}

			fizzBuzzWriter.close();
		} catch (FileNotFoundException e) 
		{
			
		}
		System.out.println("FizzBuzz.txt has been created.");
	}
}
