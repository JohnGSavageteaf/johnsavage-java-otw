package com.techelevator;

public class BankAccount 
{
private String accountHolderName;
private String accountNumber;
protected int balance;

public BankAccount (String accountHolderName, String accountNumber)
	{
			this.accountHolderName=accountHolderName;
			this.accountNumber=accountNumber;
			this.balance=0;
	}	
	
public BankAccount (String accountHolderName, String accountNumber, int balance)
	{
			this.accountHolderName=accountHolderName;
			this.accountNumber=accountNumber;
			this.balance=balance;
	}	

//getters
	
		public String getAccountHolderName()
		{
			return accountHolderName;
		}
		public String getAccountNumber()
		{
			return accountNumber;
		}
		public int getBalance()
		{
			return balance;
		}

//Method
		
		public int deposit(int amountToDeposit)
		{
			amountToDeposit = amountToDeposit + balance;
			
			balance = amountToDeposit;
			
			return amountToDeposit;
			

		}
		
		public int withdraw(int amountToWithdraw)
		{
		
			 amountToWithdraw = balance -amountToWithdraw;
			 
			 balance = amountToWithdraw;
			 
			 return balance; 
					 
/*					 @Override
			    public int withdraw(int amountToWithdraw) 
			    {
			        // only perform transaction of positive $ and room for fee
			        if (getBalance() - amountToWithdraw >= 2) 
			        {
			            super.withdraw(amountToWithdraw);
			            // Assess $2 fee if it goes below $150
			            if (getBalance() < 150) 
			        {
			                super.withdraw(2);
			        }
			     }
			        return getBalance();
*/
		}	
		



}
