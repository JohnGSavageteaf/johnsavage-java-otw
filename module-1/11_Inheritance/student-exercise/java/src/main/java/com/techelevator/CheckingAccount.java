package com.techelevator;

public class CheckingAccount extends BankAccount
{

	public CheckingAccount(String accountHolderName, String accountNumber, int balance) 
	{
		super(accountHolderName, accountNumber, balance);
		// TODO Auto-generated constructor stub
	}

	
	 public CheckingAccount(String accountHolderName, String accountNumber)
	 {
		 super(accountHolderName, accountNumber);	 
	 }
	
	
	 public int withdraw(int amountToWithdraw)
		{
			if (balance < amountToWithdraw)
			{
				return balance;
			}
			
			return balance;
		}
}