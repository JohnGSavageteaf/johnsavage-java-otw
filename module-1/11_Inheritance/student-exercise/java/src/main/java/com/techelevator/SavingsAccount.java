package com.techelevator;

public class SavingsAccount extends BankAccount
{

	public SavingsAccount(String accountHolderName, String accountNumber, int balance) 
	{
		super(accountHolderName, accountNumber, balance);
		// TODO Auto-generated constructor stub
	}
	 public SavingsAccount(String accountHolderName, String accountNumber)
	 {
		 super(accountHolderName, accountNumber);
		 
	 }
	 
	 

	public int withdraw(int amountToWithdraw)
	{
		if (balance < amountToWithdraw)
		{
			return balance;
		}	
		balance = balance - (amountToWithdraw);
		
		if (balance < 150)
		{
			balance = balance - 2;
		} 
		return balance ;
		
		
		
		
	}
	
}
