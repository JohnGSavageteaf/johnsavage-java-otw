package com.techelevator.ui;
import java.io.IOException;
import java.lang.String;
import com.techelevator.Items.*;
import com.techelevator.models.*;
public class UserOutput 
{
	private static String Windowscls = "cls";
	public static void WelcomeScreen()
	{
	System.out.println("**********************************************************");
	System.out.println(            "WELCOME**TO**THE**VENDO-MATIC**800"            );
	System.out.println("**********************************************************");
	}
	
	public static void displayInventory() {
			for (Items yes : Inventory.inventoryList) {
				String line = String.format("%-2s %-2s $ %-2 %-2s", yes.getSlotNumber(), yes.getName(), yes.getPrice(), yes.getType());
				System.out.printf(line);
//System.out.println(yes.getSlotNumber() + " 	 " + yes.getName() + " " + yes.getPrice() + " " + yes.getType());
			
				/*String line = String.format("%-6s %-35s $ %6.2f     %-5d $ %10.2f", id, name, price, quantity, lineTotal);

                fileWriter.println(line);
                */
				
			}
		
	}
	public static void displayNotEnoughmoney() {
		System.out.println();
		System.out.println("********************");
		System.out.println("Not enough money");
		System.out.println("********************");

	} public static void displayNotEnoughItems() {
		System.out.println();
		System.out.println("********************");
		System.out.println("There's not enough of that item to go around!");
		System.out.println("********************");

	}
	public static void exit() {
		System.out.println();
		System.out.println("Thank you for choosing Vendo-Matic 800!");
		System.out.println();
	}
	public static void space() {
		System.out.println();
		System.out.println("*************");
		System.out.println();
	}
	public final static void pseudoClearConsole()
	{
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		
	}
	public static void feedMoneyOutput() {
		System.out.println("Please select bill denominations");
		System.out.println();
		System.out.println("A) $1.00");
		System.out.println();
		System.out.println("B) $2.00");
		System.out.println();
		System.out.println("C) $5.00");
		System.out.println();
		System.out.println("D) $10.00");
		System.out.println();
		System.out.println("E) Select Product");
		System.out.println();
		System.out.println("Current Money Provided: $" +  UserInput.MoneyProvided);
	}
	public static void purchaseMenuOutput() {
		System.out.println("A) Feed Money");
		System.out.println();
		System.out.println("B) Select Product");
		System.out.println();
		System.out.println("C) Finish Transaction");
		System.out.println();
		System.out.println("Current Money Provided: $" +  UserInput.MoneyProvided);
		System.out.println();
		System.out.println("Please select an option");
		System.out.println();
	}
	public static void mainMenuOutput() {
		space();
		System.out.println("A) Display Vending Machine Items");
		System.out.println();
		System.out.println("B) Purchase");
		System.out.println();
		System.out.println("C) Exit");
		System.out.println();
		System.out.println("Please select an option");
		System.out.println();
	}
	public static void displayCurrentMoneyProvided() {
		System.out.println("Current Money Provided: $" + UserInput.MoneyProvided);
	}
	
}
