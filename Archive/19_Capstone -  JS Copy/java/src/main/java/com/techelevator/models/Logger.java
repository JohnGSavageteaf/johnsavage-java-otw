package com.techelevator.models;

import com.techelevator.Items.*;
import com.techelevator.ui.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import com.techelevator.ui.UserInput;

public class Logger {
	private static File output = new File("Log.txt");
	private final static String currentDate = LocalDate.now().format(DateTimeFormatter.ISO_DATE); // YYYY-MM-DD
	private final static String currentTime = LocalTime.now().format(DateTimeFormatter.ISO_TIME);

	// private final static String startBalance = String.valueOf(startingBalance);
	// private final static String endBalance = String.valueOf(endingBalance);
	private static void writeToFile(String finished) {
		try (FileOutputStream outputStream = new FileOutputStream(output, true);
				PrintWriter writer = new PrintWriter(outputStream);) {
			writer.println(finished);
			writer.flush();
		} catch (Exception e) {
			System.out.println("File was not found");
		}
	}

	public static void logFeed(double d, double amountFed) {

		String endBalance = String.valueOf(d);
		String startTransaction = "FEED MONEY: ";
		String finished = currentDate + " " + currentTime + " " + startTransaction + " $" + d +"0"
				+ " $" + amountFed + "0";
		writeToFile(finished);
	}

	public static void giveChange(double startingBalance, double endingBalance) {
		String change = "GIVE CHANGE :";
		String finished = currentDate + " " + currentTime + " $" + change + String.valueOf(startingBalance + "0") + " $"
				+ String.valueOf(endingBalance + "0");
		writeToFile(finished);
	}

	public static void purchaseFeed(Items items, String slotNumber,BigDecimal test, double endingBalance) {
		String itemName = items.getName() + " ";
		String finished = currentDate + " " + currentTime + " " + itemName + " " + slotNumber + " "  + test + " " + String.valueOf(endingBalance);
		writeToFile(finished);
	}
	

}
