package com.techelevator.Items;

import java.math.BigDecimal;

public class Items {
	String slotNumber;
	String name;
	double price;
	String type;
	int quantity = 5;
	
	public Items(String slotNumber, String name, double price, String type) {
		this.slotNumber = slotNumber;
		this.name = name;
		this.price = price;
		this.type = type;
				
	}
	public String getSlotNumber() {
		return slotNumber;
	}
	public String getName() {
		return name;
	}
	public double getPrice() {
		return price;
	}
	public String getType() {
		return type;
	}
	public void message() {
		
	}
	public int dispenser(int dispensed) {
		quantity -= dispensed;
		
		return quantity;
	}
	public int getQuantity() {
		return quantity;
	}
	
	
}
