package com.techelevator.application;
import com.techelevator.ui.*;
import com.techelevator.models.*;
public class VendingMachine 
{
    public void run()
    {
    	
        while(true)
        {	Inventory.createList();
     
            String choice = UserInput.mainMenu();

            if(choice.equals("display"))
            {
                // display the vending machine slots
            	UserOutput.displayInventory();
            	
            }
            else if(choice.equals("purchase"))
            {
                UserInput.purchaseMenu(); 
            }
            else if(choice.equals("exit"))
            {
                UserOutput.exit();
                break;
            }
        }
    }
    
}
