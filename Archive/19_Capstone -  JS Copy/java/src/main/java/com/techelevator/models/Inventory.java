package com.techelevator.models;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.*;
import com.techelevator.Items.*;

public class Inventory {
	public static List<Items> inventoryList = new ArrayList<>();
	private static File inventory = new File("vendingmachine.csv");

	public static void createList() {
		try (Scanner csvInventory = new Scanner(inventory)) {

			while (csvInventory.hasNextLine()) {
				String line = csvInventory.nextLine();
				String[] part = line.split("\\|");
				String slotNumber = part[0];
				String name = part[1];
				double price = Double.parseDouble(part[2]);
				String type = part[3];

				if (slotNumber.startsWith("A")) {
					inventoryList.add(new Chips(slotNumber, name, price, type));
				} else if (slotNumber.startsWith("B")) {
					inventoryList.add(new Candy(slotNumber, name, price, type));
				} else if (slotNumber.startsWith("C")) {
					inventoryList.add(new Drink(slotNumber, name, price, type));
				} else if (slotNumber.startsWith("D")) {
					inventoryList.add(new Gum(slotNumber, name, price, type));
				}

				// Items items = new Items(slotNumber, name, price, type);
				// inventoryList.add(items); ALSO WORKS. WHICH IS BETTER??!?!
			}
		} catch (FileNotFoundException e) {// TODO Auto-generated catch block
			System.out.println("File not found");
		}

	}
}

