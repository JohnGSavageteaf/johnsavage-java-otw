package com.techelevator.ui;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Scanner;
import com.techelevator.ui.*;
import javax.print.attribute.standard.OutputDeviceAssigned;
import com.techelevator.Items.Items;
import com.techelevator.models.Inventory;
import com.techelevator.models.Logger;

public class UserInput {
	private static Scanner scanner = new Scanner(System.in);
	public static Double currentMoneyProvided = 0.00;
	public static BigDecimal MoneyProvided = new BigDecimal(0.00);

	public static String mainMenu() {
		UserOutput.mainMenuOutput();
		String selection = scanner.nextLine();
		String option = selection.trim().toUpperCase();

		if (option.equals("A")) {
			return "display";
		} else if (option.equals("B")) {
			return "purchase";
		} else if (option.equals("C")) {
			return "exit";
		} else {
			return "";
		}

	}

	public static void purchaseMenu() {

		UserOutput.purchaseMenuOutput();
		String selection = scanner.nextLine();

		String option = selection.trim().toUpperCase();

		if (option.equals("A")) {
			feedMoney();
		}
		// CALL THE PAYMENT METHOD HERE

		else if (option.equals("B")) {
			SelectItem();
		}
		// CALL THE SELECT METHOD HERE
		else if (option.equals("C")) {
			returnCurrentMoney();
		} else {
			System.out.println("Please select a valid option");
			purchaseMenu();
		}

	}

	private static void feedMoney() {
		UserOutput.pseudoClearConsole();
		while (true) {

			UserOutput.feedMoneyOutput();
			String selection = scanner.nextLine();

			String option = selection.trim().toUpperCase();
			double startingAmount = currentMoneyProvided;

			if (option.equals("A")) {
				currentMoneyProvided += 1.00;
				Logger.logFeed(1.00, currentMoneyProvided);
			}
			// CALL THE PAYMENT METHOD HERE

			else if (option.equals("B")) {
				currentMoneyProvided += 2.00;
				Logger.logFeed(2.00, currentMoneyProvided);
			}
			// CALL THE SELECT METHOD HERE
			else if (option.equals("C")) {
				currentMoneyProvided += 5.00;
				Logger.logFeed(5.00, currentMoneyProvided);
			} else if (option.equals("D")) {
				currentMoneyProvided += 10.00;
				Logger.logFeed(10.00, currentMoneyProvided);
			}
			// CALL THE FINISH TRANSACTION METHOD HERE
			else if (option.contentEquals("E")) {
				SelectItem();
				break;
			}

			MoneyProvided = BigDecimal.valueOf(currentMoneyProvided).setScale(2, RoundingMode.HALF_UP);
			System.out.println("Money " + MoneyProvided);
			UserOutput.displayCurrentMoneyProvided();
		}

	}

	private static void SelectItem() {
		UserOutput.displayInventory();
		final int itemsRemoved = 1;
		System.out.println(
				"Please enter the slot number for your selection or enter \"L\" to return to the purchase menu");
		String selection = scanner.nextLine();
		String option = selection.trim().toUpperCase();
		double beginningBalance = currentMoneyProvided;
		BigDecimal test = BigDecimal.valueOf(beginningBalance).setScale(2, RoundingMode.HALF_UP);
		for (Items item : Inventory.inventoryList) {
			if (item.getSlotNumber().equals(option)) {
				if (item.getQuantity() > 0 && currentMoneyProvided >= item.getPrice()) {
					currentMoneyProvided -= item.getPrice();
					double endingBalance = currentMoneyProvided - item.getPrice();
					UserOutput.space();
					item.dispenser(itemsRemoved);
					item.message();
					MoneyProvided = BigDecimal.valueOf(currentMoneyProvided).setScale(2, RoundingMode.HALF_UP);
					Logger.purchaseFeed(item, item.getSlotNumber(), test, endingBalance);
				} else if (item.getQuantity() <= 0) {
					UserOutput.space();
					UserOutput.displayNotEnoughItems();

				} else if (currentMoneyProvided < item.getPrice()) {
					UserOutput.space();
					UserOutput.displayNotEnoughmoney();

				}
				purchaseMenu();
			} else if (option.equals("L")) {
				UserOutput.space();
				purchaseMenu();
			}

		}
	}

	public static void returnCurrentMoney() {

		int quarter, dime, nickel, change;

		change = (int) (Math.ceil(currentMoneyProvided * 100));

		quarter = Math.round((int) change / 25);

		change = change - quarter * 25;

		dime = Math.round((int) change / 10);

		change = change - dime * 10;

		nickel = Math.round((int) change / 5);

		change = change - nickel * 5;

		System.out.println((quarter) + " quarters");
		System.out.println(dime + " dimes");
		System.out.println(nickel + " nickels");
		Logger.giveChange(currentMoneyProvided, 0.00);
		currentMoneyProvided = 0.00;
		MoneyProvided = BigDecimal.valueOf(currentMoneyProvided).setScale(2, RoundingMode.HALF_UP);
		System.out.println(MoneyProvided);

	}

}
