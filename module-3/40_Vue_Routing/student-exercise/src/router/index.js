import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import myBooks from '../views/MyBooks.vue';
import addABook from '../views/NewBook.vue';


Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/myBooks',
    name: 'myBooks',
    component: myBooks
  },
  {
    path: '/addBook',
    name: 'newBook',
    component: addABook
  },

];

const router = new VueRouter({
  mode: 'history',
  routes
});

export default router;
